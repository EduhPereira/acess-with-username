import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";
import { GetUserComponent } from "./components/GetUserComponent";
import { WelcomePage } from "./components/WelcomePage";

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [user, setUser] = useState("");

  return (
    <div className="App">
      {isLoggedIn ? (
        <header className="App-header">
          <WelcomePage user={user} setIsLoggedIn={setIsLoggedIn} />
        </header>
      ) : (
        <header className="App-header">
          <GetUserComponent setUser={setUser} setIsLoggedIn={setIsLoggedIn} />
        </header>
      )}
    </div>
  );
}

export default App;
