import "./styles.css";
import { useState } from "react";

export const GetUserComponent = ({ setUser, setIsLoggedIn }) => {
  const [userInput, setUserInput] = useState("");
  const HandleLogin = (userInput) => {
    setIsLoggedIn(true);
    setUser(userInput);
  };

  return (
    <form className="Form" action="#">
      <input
        type="text"
        value={userInput}
        className="UserInput"
        placeholder="Insert a Username Here"
        onChange={(event) => setUserInput(event.target.value)}
      />
      <button className="LoginBtn" onClick={() => HandleLogin(userInput)}>
        Acessar com o nome
      </button>
    </form>
  );
};
