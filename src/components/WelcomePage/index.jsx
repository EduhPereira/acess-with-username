import "./style.css";

export const WelcomePage = ({ user, setIsLoggedIn }) => {
  const HandleLogout = () => {
    setIsLoggedIn(false);
  };

  return (
    <div>
      <h1 className="UserName">welcome {user}</h1>
      <button className="LogoutBtn" onClick={HandleLogout}>
        Logout
      </button>
    </div>
  );
};
